# Vydia bot # 


## Structure of Repository

* install_all.sh : Script to install all dependencies
* config.static.json: rename to config.json and put facebook_api_key, dialog_flow_api_key and dialog_flow_project_id
* main.py: Contains all the logic
* data/*: Data for each of the users. Contains the medium posts in a pkl panda object
* models/*: Trained model for each of the users. 

## How to run ##

* bash run.sh

The script will start the flask server

