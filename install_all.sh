virtualenv -p python3.6 python3.6_venv
source python3.6_venv/bin/activate
pip3 install -r requirements.txt
mkdir data
mkdir models