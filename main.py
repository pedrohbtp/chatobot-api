from flask import Flask
from flask_cors import CORS, cross_origin
from flask import Response
import flask
import json
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize
import pickle
import os
import pandas as pd
import requests
import config
# import dialogflow
import dialogflow_v2 as dialogflow
import apiai

app = Flask(__name__)

DATA_PATH = 'data/'
MODEL_PATH= 'models/'
FACEBOOK_MESSAGES = 'https://graph.facebook.com/v2.6/me/messages'
FACEBOOK_API_KEY=config.facebook_api_key
DIALOG_FLOW_API_KEY=config.dialog_flow_api_key
DIALOG_FLOW_PROJECT_ID=config.dialog_flow_project_id

from gensim.models.doc2vec import Doc2Vec
import pickle
import pandas as pd
from gensim.parsing import preprocessing

def pickle_obj(data, path):
    with open(path,'wb+') as f:
        pickle.dump(data,f)

def unpickle_obj(path):
    try:
        with open(path,'rb') as f:
            data = pickle.load(f)
    except:
        data = pd.DataFrame()
    return data



def update_data_and_model(new_data_list, data_path, model_path, max_epochs=100, vec_size=20, alpha=0.025):
    ''' Receives a new list representing the data and retrains the model 
    new_data_list: a list of json object like [{'text': , 'link': }]
    '''
    # loads the panda dataframe
    panda_data = unpickle_obj(data_path)
    # transforms the list of json into dataframe
    new_data = pd.DataFrame(new_data_list)
    # extends the dataframe
    new_data.append(panda_data)
    pickle_obj(new_data, data_path)
    # retrains the model
    model = train_model(new_data['text'].tolist(), max_epochs=max_epochs, vec_size=vec_size, alpha=alpha)
    model.save(model_path)
    return model

def train_model(data, max_epochs=100, vec_size=20, alpha=0.025):
    ''' data: list of strings used as input
    Returns: trained model
    '''
    tagged_data = [TaggedDocument(words=preprocessing.preprocess_string(text_data), tags=[str(i)]) for i, text_data in enumerate(data)]
    model = Doc2Vec(size=vec_size,
                    alpha=alpha, 
                    min_alpha=0.00025,
                    min_count=1,
                    dm =1)

    model.build_vocab(tagged_data)
    # loops over the data and trains the model
    for epoch in range(max_epochs):
        model.train(tagged_data,
                    total_examples=model.corpus_count,
                    epochs=model.iter)
        # decrease the learning rate
        model.alpha -= 0.0002
        # fix the learning rate, no decay
        model.min_alpha = model.alpha
    return model
    
    
def search_doc(new_document, model_path, data_path):
    ''' new_document: String to use to search 
    '''
    model= Doc2Vec.load(model_path)
    # unpickles the data to retrieve the most similar doc
    data = unpickle_obj(data_path)
    # tokenize to use doc2vec first
    tokenized_data = preprocessing.preprocess_string(new_document)
    # Get the vec representation of the new doc
    v1 = model.infer_vector(tokenized_data)
    # get the most similar doc in training 
    similar_doc = model.docvecs.most_similar(positive=[v1], topn=1)
    # gets the data index of the most similar document
    doc_index = similar_doc[0][0]
    # Returns the most similar doc
#     print(model, doc_index)
#     print(' data: ', data)
    return {'text': data['text'][int(doc_index)], 'link': data['link'][int(doc_index)]}

@app.route('/search-post',methods=['POST'])
def search_post():
    ''' 
    POST: Searches among the available posts and return the most relevant
    '''
    form_data = flask.request.get_json()
    user = form_data['user']
    data = form_data['text']
    data_path = DATA_PATH+user+'.pkl'
    model_path = MODEL_PATH+user+'.pkl'
    # print('form data: ', form_data)
    # print('data path: ', data_path, os.path.isfile(data_path))
    # print(' model path: ', model_path, os.path.isfile(model_path))
    if os.path.isfile(data_path) and os.path.isfile(model_path):
        response = search_doc(data, model_path = model_path, data_path = data_path)
        resp = Response(response=json.dumps({"response": response}), status=200, mimetype='application/json')
    else:
        resp = Response(response=json.dumps({"response": 'user not found'}), status=404, mimetype='application/json')
    return resp


@app.route('/add-documents',methods=['POST'])
def add_document():
    ''' 
    POST: Adds a new document to the database
    '''
    form_data = flask.request.get_json()
    print('form data: ', form_data)
    #TODO: add the loginc to add the document to our database
    resp = Response(response=json.dumps({"response": 'hello add document'}), status=200, mimetype='application/json')
    return resp

@app.route('/process-message',methods=['POST'])
def process_message():
    ''' 
    POST: Receives raw message and processes it
    '''
    
    form_data = flask.request.get_json()
    # print('form data: ', form_data)
    # verify_challenge = flask.request.args['hub.challenge']
    recipient_id = form_data['entry'][0]['messaging'][0]['sender']['id']
    google_flow_response = dialog_flow_talk(session_id = recipient_id, text=form_data['entry'][0]['messaging'][0]['message']['text'])
    #TODO: send back to facebook or query first based on the question
    print('flow response: ', google_flow_response)
    if 'user.search1' in google_flow_response['intent_name'] or 'user.search2' in google_flow_response['intent_name']:
        text_response = 'check this doc: ' + search_doc(google_flow_response['text'], model_path = 'models/tom.pkl', data_path = 'data/tom.pkl')['link']
    else:
        text_response = google_flow_response['text']
    
    post_resp = requests.post(FACEBOOK_MESSAGES+'?access_token='+FACEBOOK_API_KEY, headers = {'content-type': 'application/json'}, 
    data = json.dumps({'recipient': {'id': recipient_id}, 'message': {"text": text_response}}) )
    
    # print('respo: ', post_resp.content)
    resp = Response(response=json.dumps({"response": 'successfully sent reply'}), status=200, mimetype='application/json')
    return resp

def dialog_flow_talk(session_id, text, language='en'):
    ''' gets the user session id and text and returns the reponse given by dialog flow 
    '''
    ai = apiai.ApiAI(DIALOG_FLOW_API_KEY)
    request = ai.text_request()
    request.session_id = session_id
    request.query = text    
    request.lang = language
    response = request.getresponse()
    google_flow_response = json.loads(response.read().decode("utf-8"))
    print('\ngoogle flow response: ', google_flow_response)
    text_resp = google_flow_response['result']['fulfillment']['speech'] if 'result' in google_flow_response else ''
    intent_name = google_flow_response['result']['metadata']['intentName'] if 'result' in google_flow_response and 'metadata' in google_flow_response['result'] and 'intentName' in google_flow_response['result']['metadata'] else ''
    return {'text': text_resp, 'intent_name': intent_name}

@app.route('/dialog-flow',methods=['POST'])
def dialog_flow():
    ''' 
    POST: communication with dialog flow
    '''
    form_data = flask.request.get_json()
    google_flow_resp = dialog_flow_talk(session_id=form_data['session_id'], text=form_data['text'] )
    resp = Response(response=json.dumps({"response": google_flow_resp['text']}), status=200, mimetype='application/json')
    return resp

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8082)
